/*
 *  Fenix - Videogame compiler/interpreter
 *  Current release       : FENIX - PROJECT 1.0 - R 0.84
 *  Last stable release   :
 *  Project documentation : http://fenix.divsite.net
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Copyright � 1999 Jos� Luis Cebri�n Pag�e
 *  Copyright � 2002 Fenix Team
 *
 */

#ifndef _FNX_LOADLIB_H
    #define _FNX_LOADLIB_H

    /* WIN32 INCLUDES */
    #ifdef WIN32
        #include <windows.h>
        #include <winbase.h>
    #endif
    #ifdef TARGET_MAC
        #include <dlfcn.h>
        #include <unistd.h>
        #define __stdcall
    #endif
     #ifdef TARGET_Linux
        #include <dlfcn.h>
        #include <unistd.h>
        #define __stdcall
    #endif
     #ifdef TARGET_LinuxPPC
        #include <dlfcn.h>
        #include <unistd.h>
        #define __stdcall
    #endif
    #ifdef TARGET_BEOS
        #include <unistd.h>
	#include <dlfcn.h>
    #endif
    #ifdef TARGET_BSD
    	#include <unistd.h>
	#include <dlfcn.h>
	#define __stdcall
    #endif

    typedef void (__stdcall  * dlfunc) (void *(*)(const char *), int (*)(char *, char *, int, void *));

    #ifdef WIN32
        #define dlopen(a,b)     LoadLibrary(a)
        #define dlsym(a,b)      (dlfunc)GetProcAddress(a,b)

        static char * dlerror (void)
        {
            static char * buffer = "Could not load library." ;
            return buffer;
        }
    #endif

#endif
